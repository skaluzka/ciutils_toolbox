#!/bin/env python2

from setuptools import setup

setup(
    name='ciutils_toolbox',
    version='0.1.0',
    description='',
    zip_safe=False,
    packages=['ciutils_toolbox'],
    install_requires=[
       'ciutils_core',
    ],
    dependency_links=[
        'https://bitbucket.org/skaluzka/ciutils_core/get/master.tar.gz#egg=ciutils_core',
    ],
    scripts=[
        'ciutils_toolbox/bashtool1.sh',
        'ciutils_toolbox/bashtool2.sh',
    ],
)


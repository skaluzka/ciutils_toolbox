#!/bin/bash
#set -x


if [ -f ./set_proxy.sh ] ; then
    source ./set_proxy.sh
fi

#deleting old stuff
rm -fv jenkins*.war

echo "fetching latest jenkins version from html, please wait..."
HTML_LINE=$(curl -s http://mirrors.jenkins-ci.org/war/ | grep "<tr><td" | tail -2 | head -1)
echo "HTML_LINE=$HTML_LINE"
LATEST=$(echo $HTML_LINE | sed "s/^.*>\(1\.[0-9][0-9][0-9]\).*$/\1/g")
echo "LATEST=$LATEST"
echo "downloading latest jenkins version, please wait..."
wget http://mirrors.jenkins-ci.org/war/latest/jenkins.war -O jenkins.latest.$LATEST.war
ln -s jenkins.latest.$LATEST.war jenkins.war

echo "fetching latest LTS jenkins version from html, please wait..."
HTML_LINE=$(curl -s http://mirrors.jenkins-ci.org/war-stable/ | grep "<tr><td" | tail -2 | head -1)
echo "HTML_LINE=$HTML_LINE"
LATEST_LTS=$(echo $HTML_LINE | sed "s/^.*>\(1\.[0-9][0-9][0-9]\.[0-9]\).*$/\1/g")
echo "LATEST_LTS=$LATEST_LTS"
echo "downloading latest LTS jenkins version, please wait..."
wget http://mirrors.jenkins-ci.org/war-stable/latest/jenkins.war -O jenkins.lts.latest.$LATEST_LTS.war
ln -s jenkins.lts.latest.$LATEST_LTS.war jenkins.lts.war

ls -al jenkins*

echo "done"

#todo:
# - cli support for specified jenkins version (latest by default)
# - cli param -v/--verbose


#!/bin/bash
#set -x


if [ -f ./set_proxy.sh ] ; then
    source ./set_proxy.sh
fi

echo "downloading latest jenkins version, please wait..."
wget http://mirrors.jenkins-ci.org/war/latest/jenkins.war -O jenkins.war
unzip jenkins.war -d clean_jenkins
rm -v clean_jenkins/WEB-INF/plugins/*
cd clean_jenkins
zip -r ../clean_jenkins.war *
cd ..
rm -rf clean_jenkins

echo "downloading latest LTS jenkins version, please wait..."
wget http://mirrors.jenkins-ci.org/war-stable/latest/jenkins.war -O jenkins.lts.war
unzip jenkins.lts.war -d clean_jenkins_lts
rm -v clean_jenkins_lts/WEB-INF/plugins/*
cd clean_jenkins_lts
zip -r ../clean_jenkins_lts.war *
cd ..
rm -rf clean_jenkins_lts

echo "done"

#todo:
# - use separated script for downloading


#!/bin/env python2
# -*- coding: utf-8 -*-


import argparse
import os
import sys
import urllib2
import xml.etree.ElementTree as et
from xml.dom import minidom


class JenkinsPluginFetcher(object):


    def __init__(self, url, xml_file_path, txt_file_path):
        self.url = url + '/pluginManager/api/xml?depth=2'
        self.xml_file_path = xml_file_path
        self.txt_file_path = txt_file_path
        self.plugin_list = []


    def download_plugin_list_to_xml_file(self):
        try:
            page_content = urllib2.urlopen(self.url).read()
            pretty_page_content = minidom.parseString(page_content).toprettyxml(indent='    ', newl=os.linesep)
            open(self.xml_file_path, "w").write(pretty_page_content)
            print(os.linesep+'url "%s" content was successfully saved to "%s" file' % (self.url, self.xml_file_path))
        except:
            print('error! an exception %s occured: %s' % (sys.exc_info()[0], sys.exc_info()[1]))
            if str(sys.exc_info()[1]) == 'HTTP Error 404: Not Found':
                print('could not find page: %s' % self.url)
            sys.exit(1)


    def parse_xml_file(self):
        try:
            tree = et.parse(self.xml_file_path)
            tree_root = tree.getroot()
        except et.ParseError as ex:
            print 'error %s' % ex.message
            sys.exit(1)
        for r in tree_root.iter('plugin'):
            plugin_dict = {}
            for k in ('shortName', 'longName', 'url', 'active', 'bundled', 'downgradable', 'enabled', 'hasUpdate', 'pinned', 'supportsDynamicLoad', 'version'):
                plugin_dict[k] = r.find(k).text
            deps = []
            for dep in r.findall('dependency'):
                deps.append({'optional': dep.find('optional').text,
                             'shortName': dep.find('shortName').text,
                             'version': dep.find('version').text})
            plugin_dict['deps'] = deps
            self.plugin_list.append(plugin_dict)


    def save_results_to_txt_file(self):
        with open(self.txt_file_path, 'w') as f:
            for i in self.plugin_list:
                dep_str = ''
                for d in i['deps']:
                    dep_str += 'shortName = %s; optional = %s; version = %s' % (d['shortName'], d['optional'], d['version'])
                    dep_str += os.linesep
                plugin_str = '''
longName = %s
shortName = %s
version = %s
url = %s
active = %s
bundled = %s
downgradable = %s
enabled = %s
hasUpdate = %s
pinned = %s
supportsDynamicLoad = %s
dependencies:
%s
''' % (i['longName'], i['shortName'], i['version'], i['url'], i['active'], i['bundled'], i['downgradable'],
        i['enabled'], i['hasUpdate'], i['pinned'], i['supportsDynamicLoad'], dep_str)
                f.write(plugin_str)




def main(cli_args_dict):
    pf = JenkinsPluginFetcher(url=cli_args_dict['url'],
                              xml_file_path=cli_args_dict['xml_output_file'],
                              txt_file_path=cli_args_dict['txt_output_file'])
    pf.download_plugin_list_to_xml_file()
    if cli_args_dict['txt_output_file']:
        pf.parse_xml_file()
        pf.save_results_to_txt_file()
    print('done')




if __name__ == '__main__':
    cli_parser = argparse.ArgumentParser()
    cli_parser.add_argument('-u',
                            '--url',
                            dest='url',
                            metavar='URL',
                            default='http://localhost:8080',
                            help='jenkins instance url (default: %(default)s)')
    cli_parser.add_argument('-x',
                            '--output-xml',
                            dest='xml_output_file',
                            metavar='XML_OUTPUT_FILE',
                            default='plugins.xml',
                            help='full path to output xml file (default: %(default)s)')
    cli_parser.add_argument('-t',
                            '--output-txt',
                            dest='txt_output_file',
                            metavar='TXT_OUTPUT_FILE',
                            help='full path to output txt file')
    cli_args = cli_parser.parse_args()
    cli_args_dict = vars(cli_args)
    main(cli_args_dict)




#todo:
# - add download option


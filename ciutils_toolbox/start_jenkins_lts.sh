#!/bin/bash
set -x


CWD=$(pwd)
HTTP_PORT=9000
HTTPS_PORT=9000

#jenkins variables
export JENKINS_HOME=${CWD}/jenkins_lts/.jenkins
PATH=${JENKINS_HOME}:${PATH}
export PATH

#for https use: --httpPort=-1 --httpsPort=${HTTPS_PORT}
java -jar jenkins.lts.war --httpPort=${HTTP_PORT}

echo "done"


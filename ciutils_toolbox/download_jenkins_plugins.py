#!/bin/env python2
# -*- coding: utf-8 -*-


import argparse
import os
import re
import sys
import urllib2
import xml.etree.ElementTree as et
import zipfile


class PluginDownloader(object):

    PLUGINS_URL = 'https://updates.jenkins-ci.org/download/plugins/'

    def __init__(self, xml_file_path, output_dir, no_deps):
        self.xml_file_path = xml_file_path
        self.output_dir = output_dir
        if not self.output_dir == '.':
            if os.path.exists(self.output_dir):
                print 'output dir already exists: %s' % self.output_dir
                sys.exit(1)
            else:
                os.makedirs(self.output_dir)
                print 'output dir created: "%s"' % self.output_dir
        self.no_deps = no_deps
        self.plugin_list = []
        self.__parse_xml_file()


    def __parse_xml_file(self):
        try:
            tree = et.parse(self.xml_file_path)
            tree_root = tree.getroot()
        except et.ParseError as ex:
            print 'error %s' % ex.message
            sys.exit(1)
        for r in tree_root.iter('plugin'):
            plugin_dict = {}
            for k in ('shortName', 'longName', 'version'):
                plugin_dict[k] = r.find(k).text
            self.plugin_list.append(plugin_dict)


    def __download_url_content(self, url, dst_file):
        #print url
        try:
            plugin = urllib2.urlopen(url).read()
            open(dst_file, "wb").write(plugin)
            print(os.linesep + 'url "%s" content was successfully saved to "%s" file' % (url, dst_file))
        except:
            print('error! an exception %s occured: %s' % (sys.exc_info()[0], sys.exc_info()[1]))
            if str(sys.exc_info()[1]) == 'HTTP Error 404: Not Found':
                print('could not find page: %s' % url)
            sys.exit(1)


    def __download_single_plugin(self, url, file_path):
        self.__download_url_content(url, file_path)


    def __extract_manifest(self, plugin_path, out_dir):
        fh = open(plugin_path, 'rb')
        z = zipfile.ZipFile(fh)
        z.extract('META-INF/MANIFEST.MF', out_dir)
        fh.close()


    def __download_plugin_deps(self, plugin_dir, plugin_name):
        deps_dir = os.path.join(plugin_dir, 'deps')
        self.__extract_manifest(plugin_path=os.path.join(plugin_dir, plugin_name),
                                out_dir=deps_dir)
        text_line = ''.join(open(os.path.join(deps_dir, 'META-INF/MANIFEST.MF')).readlines())
        text_line = text_line.replace('\r', '').replace('\n', '')
        # print text_line
        regex = re.compile("^.*Plugin-Dependencies:(.*)Plugin-Developers.*$")
        plugin_deps = regex.findall(text_line)
        if plugin_deps:
            plugin_deps = plugin_deps[0].replace(' ', '').split(',')
            required_deps = [n for n in plugin_deps if ';resolution:=optional' not in n]
            optional_deps = [n.replace(';resolution:=optional', '') for n in plugin_deps if ';resolution:=optional' in n]
            for k, v in {'required': required_deps, 'optional': optional_deps}.iteritems():
                if v:
                    print '%s dependencies = %s' % (k, v)
                    os.makedirs(os.path.join(deps_dir, k))
                for i in v:
                    plugin_name, plugin_version = i.split(':')
                    plugin_dir = os.path.join(os.path.join(deps_dir, k), plugin_name, plugin_version)
                    os.makedirs(plugin_dir)
                    plugin_path = os.path.join(plugin_dir, plugin_name + '.hpi')
                    url = self.PLUGINS_URL + plugin_name + '/' + plugin_version + '/' + plugin_name + '.hpi'
                    self.__download_url_content(url, plugin_path)
        else:
            print 'no dependencies found for this plugin...'


    def download_all_plugins(self):
        for plugin in self.plugin_list:
            plugin_name = plugin['shortName'] + '.hpi'
            plugin_dir = os.path.join(self.output_dir, plugin['shortName'], plugin['version'])
            os.makedirs(plugin_dir)
            plugin_path = os.path.join(plugin_dir, plugin_name)
            url = self.PLUGINS_URL + plugin['shortName'] + '/' + plugin['version'] + '/' + plugin_name
            self.__download_single_plugin(url, plugin_path)
            if not self.no_deps:
                print 'downloading dependencies for "%s (%s)" plugin, please wait...' % (plugin['longName'], plugin['shortName'])
                self.__download_plugin_deps(plugin_dir, plugin_name)




def main(cli_args_dict):
    pd = PluginDownloader(xml_file_path=cli_args_dict['input_xml_file'],
                          output_dir=cli_args_dict['output_dir'],
                          no_deps=cli_args_dict['no_deps'])
    pd.download_all_plugins()
    print('done')




if __name__ == '__main__':
    cli_parser = argparse.ArgumentParser()
    cli_parser.add_argument('-x',
                            '--input-xml',
                            dest='input_xml_file',
                            metavar='INPUT_XML_FILE',
                            default='plugins.xml',
                            help='full path to input xml file (default: %(default)s)')
    cli_parser.add_argument('-d',
                            '--output-dir',
                            dest='output_dir',
                            metavar='OUTPUT_DIR',
                            default='plugins',
                            help='full path to output dir (default: %(default)s)')
    cli_parser.add_argument('--no-deps',
                            dest='no_deps',
                            action='store_true',
                            help='do not download plugin dependencies')
    cli_args = cli_parser.parse_args()
    cli_args_dict = vars(cli_args)
    main(cli_args_dict)




#todo:
# - add -u/--url option for reading xml plugin list directly from jenkins

